FROM tomcat:7-jre8

RUN wget https://github.com/jwilder/dockerize/releases/download/v0.6.0/dockerize-linux-amd64-v0.6.0.tar.gz \
&& tar -C /usr/local/bin -xzvf dockerize-linux-amd64-v0.6.0.tar.gz \
&& rm -fr dockerize*.tar.gz

RUN apt-get update \
&& apt-get install -y locales \
&& apt-get clean \
&& rm -Rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN groupadd toolkit -g 999 && useradd toolkit -u 999 -g 999 -m -s /bin/bash && \
locale-gen en_US.UTF-8 && localedef -i en_GB -f UTF-8 en_US.UTF-8

RUN chown -R toolkit.toolkit /usr/local/tomcat

USER toolkit

RUN mkdir -p /usr/local/tomcat/modules && \
cd /usr/local/tomcat/modules \
&& wget https://dl.bintray.com/librehealth/lh-toolkit-legacyui/legacyui-1.4.0.omod \
&& wget https://dl.bintray.com/openmrs/omod/webservices.rest-2.17.omod \
&& wget https://dl.bintray.com/openmrs/omod/owa-1.9.0.omod \
&& cd ../webapps \
&& wget https://dl.bintray.com/librehealth/lh-toolkit-war/lh-toolkit.war

CMD ["dockerize","-wait","tcp://db:3306","-timeout","90s","catalina.sh","run"]
